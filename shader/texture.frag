#version 330

in vec2 pass_texcoord;
out vec4 out_color;
uniform sampler2D texture;

void main()
{
	out_color = vec4(texture2D(texture, pass_texcoord).r);
}
