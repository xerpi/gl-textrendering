#ifndef TEXTURE_ATLAS_H
#define TEXTURE_ATLAS_H

#include <unordered_map>
#include "Texture.hpp"
#include "BinPack2D.hpp"

class TextureAtlas {
public:
	TextureAtlas(int width, int height, GLint internalFormat);
	~TextureAtlas();

	struct GlyphInfo {
		int bitmap_left;
		int bitmap_top;
		int advance_x;
		int advance_y;
		int glyph_size;
	};

	struct HtabEntry {
		BinPack2D::Rectangle rect;
		GlyphInfo glyph_info;
	};

	int insert(unsigned int character,
		const BinPack2D::Size &size,
		const GlyphInfo &glyph_info,
		BinPack2D::Position &inserted_pos);

	int exists(unsigned int character);
	int get(unsigned int character, BinPack2D::Rectangle *rect, GlyphInfo *glyph_info);

	void updatePartTexture(GLint xoffset, GLint yoffset, GLsizei width,
		GLsizei height, GLenum format, GLenum type, const GLvoid *pixels);
	void bindTexture(GLenum target);

	GLsizei getTextureWidth() const;
	GLsizei getTextureHeight() const;

private:
	Texture *texture;
	BinPack2D *binpack2d;
	std::unordered_map<unsigned int, HtabEntry> map;
};



#endif
