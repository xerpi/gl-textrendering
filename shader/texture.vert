#version 330

uniform mat4 projection;

in vec2 position;
in vec2 texcoord;

out vec2 pass_texcoord;

void main()
{
	gl_Position = projection * vec4(position, 0.0, 1.0);
	pass_texcoord = texcoord;
}
