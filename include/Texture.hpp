#ifndef TEXTURE_H
#define TEXTURE_H

#include <epoxy/gl.h>

class Texture {
public:
	Texture();
	~Texture();

	void create(GLint internalFormat, GLsizei width, GLsizei height, GLenum format, GLenum type, const GLvoid *data);
	void updatePart(GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type, const GLvoid *pixels);
	void bind(GLenum target);

	GLsizei getWidth() const;
	GLsizei getHeight() const;

private:
	GLuint texture_id;
	GLsizei width;
	GLsizei height;
};

#endif
