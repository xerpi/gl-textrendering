#include "BinPack2D.hpp"

BinPack2D::BinPack2D(const Rectangle &rect)
{
	root = create_node(rect);
}

BinPack2D::Node *BinPack2D::create_node(const Rectangle &rect)
{
	Node *node = new Node;
	if (!node)
		return nullptr;

	node->left = nullptr;
	node->right = nullptr;
	node->rect.x = rect.x;
	node->rect.y = rect.y;
	node->rect.w = rect.w;
	node->rect.h = rect.h;
	node->filled = false;

	return node;
}

BinPack2D::~BinPack2D()
{
	free_node_recursively(root);
}

void BinPack2D::free_node_recursively(Node *node)
{
	if (node) {
		if (node->left) {
			BinPack2D::free_node_recursively(node->left);
		}
		if (node->right) {
			BinPack2D::free_node_recursively(node->right);
		}
		delete node;
	}
}

bool BinPack2D::insert(const Size &in_size, Position &out_pos, Node **out_node)
{
	return insert_node(root, in_size, out_pos, out_node);
}

bool BinPack2D::insert_node(Node *node, const Size &in_size, Position &out_pos, Node **out_node)
{
	if (node->left != nullptr || node->right != nullptr) {
		int ret = insert_node(node->left, in_size, out_pos, out_node);
		if (ret == 0) {
			return insert_node(node->right, in_size, out_pos, out_node);
		}
		return ret;
	} else {
		if (node->filled)
			return false;

		if (in_size.w > node->rect.w || in_size.h > node->rect.h)
			return false;

		if (in_size.w == node->rect.w && in_size.h == node->rect.h) {
			out_pos.x = node->rect.x;
			out_pos.y = node->rect.y;
			node->filled = true;
			if (out_node)
				*out_node = node;
			return true;
		}

		int dw = node->rect.w - in_size.w;
		int dh = node->rect.h - in_size.h;

		Rectangle left_rect, right_rect;

		if (dw > dh) {
			left_rect.x = node->rect.x;
			left_rect.y = node->rect.y;
			left_rect.w = in_size.w;
			left_rect.h = node->rect.h;

			right_rect.x = node->rect.x + in_size.w;
			right_rect.y = node->rect.y;
			right_rect.w = node->rect.w - in_size.w;
			right_rect.h = node->rect.h;
		} else {
			left_rect.x = node->rect.x;
			left_rect.y = node->rect.y;
			left_rect.w = node->rect.w;
			left_rect.h = in_size.h;

			right_rect.x = node->rect.x;
			right_rect.y = node->rect.y + in_size.h;
			right_rect.w = node->rect.w;
			right_rect.h = node->rect.h - in_size.h;
		}

		node->left = create_node(left_rect);
		node->right = create_node(right_rect);

		return insert_node(node->left, in_size, out_pos, out_node);
	}
}

bool BinPack2D::remove(Node *node)
{
	return remove_node(root, node);
}

bool BinPack2D::remove_node(Node *root, Node *node)
{
	if (root == nullptr || node == nullptr)
		return false;
	else if (root == node) {
		free_node_recursively(root->left);
		free_node_recursively(root->right);
		root->left = nullptr;
		root->right = nullptr;
		root->filled = 0;
		return true;
	}

	return remove_node(root->left, node) || remove_node(root->right, node);
}
