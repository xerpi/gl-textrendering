#include "ShaderProgram.hpp"
#include <fstream>

ShaderProgram::ShaderProgram()
{
	program_id = glCreateProgram();
}

ShaderProgram::~ShaderProgram()
{
	glDeleteProgram(program_id);
}

void ShaderProgram::addShader(GLenum shaderType, GLsizei count, const GLchar **string, const GLint *length)
{
	GLuint shader;
	GLint compile_status;
	GLint info_log_length;
	GLchar *info_log;

	shader = glCreateShader(shaderType);
	glShaderSource(shader, count, string, length);
	glCompileShader(shader);

	glGetShaderiv(shader, GL_COMPILE_STATUS, &compile_status);
	if (compile_status == GL_FALSE) {
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &info_log_length);
		info_log = new GLchar[info_log_length + 1];
		glGetShaderInfoLog(shader, info_log_length, NULL, info_log);
		throw std::runtime_error(std::string(shaderTypeToString(shaderType)) +
			" compilation failed: " + std::string(info_log));
		delete info_log;
	}

	shader_list.push_back(shader);
}

void ShaderProgram::addShaderFromFile(GLenum shaderType, const std::string &filename)
{
	std::string shader_string;
	const char *shader_c_string;
	std::ifstream file_stream(filename);
	if (!file_stream) {
		throw std::runtime_error("File " + filename + " not found.");
	}

	shader_string.assign(std::istreambuf_iterator<char>(file_stream),
		std::istreambuf_iterator<char>());

	shader_c_string = shader_string.c_str();
	addShader(shaderType, 1, &shader_c_string, NULL);
}

void ShaderProgram::link()
{
	for (GLuint shader: shader_list) {
		glAttachShader(program_id, shader);
	}

	glLinkProgram(program_id);

	for (GLuint shader: shader_list) {
		glDetachShader(program_id, shader);
		glDeleteShader(shader);
	}

	shader_list.clear();
}

void ShaderProgram::use()
{
	glUseProgram(program_id);
}

GLuint ShaderProgram::getId()
{
	return program_id;
}

const char *ShaderProgram::shaderTypeToString(GLenum shaderType)
{
	switch (shaderType) {
	case GL_VERTEX_SHADER:
		return "Vertex shader";
	case GL_FRAGMENT_SHADER:
		return "Fragment shader";
	default:
		return "Unknown";
	}
}
