#ifndef BIN_PACK_2D_H
#define BIN_PACK_2D_H


class BinPack2D {
public:

	struct Position {
		int x, y;
	};

	struct Size {
		unsigned int w, h;
	};

	struct Rectangle {
		int x, y;
		unsigned int w, h;
	};

	struct Node {
		Node *left;
		Node *right;
		Rectangle rect;
		bool filled;
	};

	BinPack2D(const Rectangle &initial_rect);
	~BinPack2D();

	bool insert(const Size &in_size, Position &out_pos, Node **out_node);
	bool remove(Node *node);

private:
	static Node *create_node(const Rectangle &rect);
	static bool insert_node(Node *node, const Size &in_size, Position &out_pos, Node **out_node);
	static bool remove_node(Node *root, Node *node);
	static void free_node_recursively(Node *node);

	Node *root;
};


#endif
