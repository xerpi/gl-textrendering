#include "Texture.hpp"

Texture::Texture()
{
	glGenTextures(1, &texture_id);
	bind(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	width = 0;
	height = 0;
}

Texture::~Texture()
{
	glDeleteTextures(1, &texture_id);
}

void Texture::create(GLint internalFormat, GLsizei width, GLsizei height, GLenum format, GLenum type, const GLvoid *data)
{
	bind(GL_TEXTURE_2D);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, format, type, data);

	this->width = width;
	this->height = height;
}

void Texture::updatePart(GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type, const GLvoid *pixels)
{
	bind(GL_TEXTURE_2D);
	glTexSubImage2D(GL_TEXTURE_2D, 0, xoffset, yoffset, width, height, format, type, pixels);
}

void Texture::bind(GLenum target)
{
	glBindTexture(target, texture_id);
}

GLsizei Texture::getWidth() const
{
	return width;
}

GLsizei Texture::getHeight() const
{
	return height;
}
