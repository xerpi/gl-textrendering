#include <iostream>
#include <epoxy/gl.h>
#include <GLFW/glfw3.h>
#include <freetype2/ft2build.h>
#include FT_CACHE_H
#include FT_FREETYPE_H
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "Texture.hpp"
#include "Font.hpp"
#include "ShaderProgram.hpp"

static void framebuffer_size_callback(GLFWwindow *window, int width, int height);
static void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods);

int main(int argc, char *argv[])
{
	GLFWwindow *window;
	GLuint color_vao;
	GLuint color_vbo[2];
	ShaderProgram *color_program;
	ShaderProgram *texture_program;
	GLint color_position_loc;
	GLint color_color_loc;
	GLint texture_position_loc;
	GLint texture_texcoord_loc;
	GLint texture_projection_mtx_loc;
	Texture *texture;
	Font *font;

	if (!glfwInit()) {
		std::cerr << "Error initializing GLFW" << std::endl;
		return -1;
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	window = glfwCreateWindow(720, 480, "GL Font rendering", NULL, NULL);
	if (!window) {
		std::cerr << "Error creating GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);

	std::cout << "OpenGL version: " << glGetString(GL_VERSION) << std::endl;

	glfwSetKeyCallback(window, key_callback);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	glm::mat4 projection_matrix = glm::ortho(0.0f, 720.0f, 480.0f, 0.0f, -1.0f, 1.0f);

	color_program = new ShaderProgram();
	color_program->addShaderFromFile(GL_VERTEX_SHADER, "../shader/color.vert");
	color_program->addShaderFromFile(GL_FRAGMENT_SHADER, "../shader/color.frag");
	color_program->link();

	texture_program = new ShaderProgram();
	texture_program->addShaderFromFile(GL_VERTEX_SHADER, "../shader/texture.vert");
	texture_program->addShaderFromFile(GL_FRAGMENT_SHADER, "../shader/texture.frag");
	texture_program->link();

	color_position_loc = glGetAttribLocation(color_program->getId(), "position");
	color_color_loc = glGetAttribLocation(color_program->getId(), "color");

	texture_position_loc = glGetAttribLocation(texture_program->getId(), "position");
	texture_texcoord_loc = glGetAttribLocation(texture_program->getId(), "texcoord");
	texture_projection_mtx_loc = glGetUniformLocation(texture_program->getId(), "projection");

	font = new Font(texture_position_loc, texture_texcoord_loc);
	font->loadFromFile("../font/Ubuntu-R.ttf");

	glGenVertexArrays(1, &color_vao);
	glBindVertexArray(color_vao);

	glGenBuffers(2, color_vbo);

	glBindBuffer(GL_ARRAY_BUFFER, color_vbo[0]);
	{
		static const GLfloat color_vertices[] = {
			 0.0f,  0.5f,
			 0.5f, -0.5f,
			-0.5f, -0.5f
		};

		glBufferData(GL_ARRAY_BUFFER, sizeof(color_vertices), color_vertices, GL_STATIC_DRAW);

		glVertexAttribPointer(color_position_loc, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(color_position_loc);
	}

	glBindBuffer(GL_ARRAY_BUFFER, color_vbo[1]);
	{
		static const GLfloat color_colors[] = {
			1.0f, 0.0f, 0.0f, 1.0f,
			0.0f, 1.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f, 1.0f,
		};

		glBufferData(GL_ARRAY_BUFFER, sizeof(color_colors), color_colors, GL_STATIC_DRAW);

		glVertexAttribPointer(color_color_loc, 4, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(color_color_loc);
	}
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	while (!glfwWindowShouldClose(window)) {
		glfwPollEvents();

		glClear(GL_COLOR_BUFFER_BIT);

		color_program->use();
			glBindVertexArray(color_vao);
				glDrawArrays(GL_TRIANGLES, 0, 3);
			glBindVertexArray(0);
		texture_program->use();
			glUniformMatrix4fv(texture_projection_mtx_loc, 1, GL_FALSE, glm::value_ptr(projection_matrix));
			font->drawText(10, 60, 0xFF0000FF, 60, "Font test");
		glUseProgram(0);

		glfwSwapBuffers(window);
	}

	glDeleteBuffers(2, color_vbo);
	glDeleteVertexArrays(1, &color_vao);

	delete color_program;
	delete texture_program;

	delete font;

	glfwDestroyWindow(window);
	glfwTerminate();

	return 0;
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}

void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GLFW_TRUE);
}
