#include <stdlib.h>
#include <string.h>
#include "TextureAtlas.hpp"

TextureAtlas::TextureAtlas(int width, int height, GLint internalFormat)
{
	BinPack2D::Rectangle rect;
	rect.x = 0;
	rect.y = 0;
	rect.w = width;
	rect.h = height;

	texture = new Texture();
	texture->create(internalFormat, width, height, GL_RED, GL_UNSIGNED_BYTE, nullptr);

	binpack2d = new BinPack2D(rect);
}

TextureAtlas::~TextureAtlas()
{
	delete texture;
	delete binpack2d;
}

int TextureAtlas::insert(unsigned int character,
			 const BinPack2D::Size &size,
			 const GlyphInfo &glyph_info,
			 BinPack2D::Position &inserted_pos)
{
	HtabEntry entry;
	BinPack2D::Node *new_node;

	if (!binpack2d->insert(size, inserted_pos, &new_node))
		return 0;

	entry.rect.x = inserted_pos.x;
	entry.rect.y = inserted_pos.y;
	entry.rect.w = size.w;
	entry.rect.h = size.h;
	entry.glyph_info = glyph_info;

	map.insert(std::make_pair(character, entry));

	return 1;
}

int TextureAtlas::exists(unsigned int character)
{
	return map.find(character) != map.end();
}

int TextureAtlas::get(unsigned int character,
		      BinPack2D::Rectangle *rect, GlyphInfo *glyph_info)
{
	std::unordered_map<unsigned int, HtabEntry>::const_iterator it;

	it = map.find(character);
	if (it == map.end())
		return 0;

	*rect = it->second.rect;
	*glyph_info = it->second.glyph_info;

	return 1;
}

void TextureAtlas::updatePartTexture(GLint xoffset, GLint yoffset, GLsizei width,
	GLsizei height, GLenum format, GLenum type, const GLvoid *pixels)
{
	texture->updatePart(xoffset, yoffset, width, height, format, type, pixels);
}

void TextureAtlas::bindTexture(GLenum target)
{
	texture->bind(target);
}

GLsizei TextureAtlas::getTextureWidth() const
{
	return texture->getWidth();
}

GLsizei TextureAtlas::getTextureHeight() const
{
	return texture->getWidth();
}
