#ifndef SHADER_PROGRAM_H
#define SHADER_PROGRAM_H

#include <epoxy/gl.h>
#include <string>
#include <vector>
#include <utility>

class ShaderProgram {
public:
	ShaderProgram();
	~ShaderProgram();

	void addShader(GLenum shaderType, GLsizei count, const GLchar **string, const GLint *length);
	void addShaderFromFile(GLenum shaderType, const std::string &filename);
	void link();
	void use();
	GLuint getId();

private:
	static const char *shaderTypeToString(GLenum shaderType);

	GLuint program_id;
	std::vector<GLuint> shader_list;
};

#endif
