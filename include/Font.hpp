#ifndef FONT_H
#define FONT_H

#include <epoxy/gl.h>
#include <string>
#include <freetype2/ft2build.h>
#include FT_CACHE_H
#include FT_FREETYPE_H
#include "TextureAtlas.hpp"

class Font {
public:
	Font(GLint texture_position_loc, GLint texture_texcoord_loc);
	~Font();

	bool loadFromFile(const std::string &filename);
	bool loadFromMemory(const void *addr, unsigned int size);

	int drawText(int x, int y, unsigned int color, unsigned int size, const char *text);

private:
	enum LoadFrom {
		FONT_LOAD_FROM_FILE,
		FONT_LOAD_FROM_MEMORY
	};

	static const unsigned int ATLAS_DEFAULT_SIZE = 512;
	static const GLint ATLAS_DEFAULT_FORMAT = GL_RED;

	static FT_Error ftc_face_requester(FTC_FaceID face_id, FT_Library library,
		FT_Pointer request_data, FT_Face *face);

	static int atlas_add_glyph(TextureAtlas *atlas, unsigned int glyph_index,
			   const FT_BitmapGlyph bitmap_glyph, int glyph_size);

	int generic_font_draw_text(bool draw,
				   int *height, int x, int y,
				   unsigned int color,
				   unsigned int size,
				   const char *text);

	TextureAtlas *tex_atlas;
	LoadFrom load_from;

	GLuint texture_vao;
	GLuint texture_vbo[2];

	FT_Library ftlibrary;
	FTC_Manager ftcmanager;
	FTC_CMapCache cmapcache;
	FTC_ImageCache imagecache;

	union {
		char *filename;
		struct {
			const void *font_buffer;
			unsigned int buffer_size;
		};
	};
};



#endif
