#include "Font.hpp"

Font::Font(GLint texture_position_loc, GLint texture_texcoord_loc)
{
	tex_atlas = new TextureAtlas(ATLAS_DEFAULT_SIZE,
		ATLAS_DEFAULT_SIZE, ATLAS_DEFAULT_FORMAT);

	glGenVertexArrays(1, &texture_vao);
	glBindVertexArray(texture_vao);

	glGenBuffers(2, texture_vbo);

	glBindBuffer(GL_ARRAY_BUFFER, texture_vbo[0]);
	{
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 2 * 4, nullptr, GL_DYNAMIC_DRAW);

		glVertexAttribPointer(texture_position_loc, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(texture_position_loc);
	}

	glBindBuffer(GL_ARRAY_BUFFER, texture_vbo[1]);
	{
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 2 * 4, nullptr, GL_DYNAMIC_DRAW);

		glVertexAttribPointer(texture_texcoord_loc, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(texture_texcoord_loc);
	}
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
}

Font::~Font()
{
	delete tex_atlas;
	glDeleteBuffers(2, texture_vbo);
	glDeleteVertexArrays(1, &texture_vao);
	FTC_FaceID face_id = (FTC_FaceID)this;
	FTC_Manager_RemoveFaceID(ftcmanager, face_id);
	FTC_Manager_Done(ftcmanager);
	if (load_from == FONT_LOAD_FROM_FILE) {
		free(filename);
	}
}

bool Font::loadFromFile(const std::string &filename)
{
	FT_Error error;

	error = FT_Init_FreeType(&ftlibrary);
	if (error != FT_Err_Ok) {
		return false;
	}

	error = FTC_Manager_New(
		ftlibrary,
		0,  /* use default */
		0,  /* use default */
		0,  /* use default */
		&ftc_face_requester,  /* use our requester */
		NULL,                 /* user data  */
		&ftcmanager);

	if (error != FT_Err_Ok) {
		FT_Done_FreeType(ftlibrary);
		return false;
	}

	this->filename = strdup(filename.c_str());

	FTC_CMapCache_New(ftcmanager, &cmapcache);
	FTC_ImageCache_New(ftcmanager, &imagecache);

	load_from = FONT_LOAD_FROM_FILE;

	return true;
}

bool Font::loadFromMemory(const void *addr, unsigned int size)
{
	return true;
}

FT_Error Font::ftc_face_requester(FTC_FaceID face_id, FT_Library library,
	FT_Pointer request_data, FT_Face *face)
{
	Font *font = (Font *)face_id;

	FT_Error error = FT_Err_Cannot_Open_Resource;

	if (font->load_from == FONT_LOAD_FROM_FILE) {
		error = FT_New_Face(
				font->ftlibrary,
				font->filename,
				0,
				face);
	} else if (font->load_from == FONT_LOAD_FROM_MEMORY) {
		error = FT_New_Memory_Face(
				font->ftlibrary,
				(const FT_Byte *)font->font_buffer,
				font->buffer_size,
				0,
				face);
	}

	return error;
}

int Font::atlas_add_glyph(TextureAtlas *atlas, unsigned int glyph_index,
	const FT_BitmapGlyph bitmap_glyph, int glyph_size)
{

	int ret;
	int i, j;
	BinPack2D::Position position;
	void *texture_data;
	unsigned int tex_width;
	const FT_Bitmap *bitmap = &bitmap_glyph->bitmap;
	unsigned int w = bitmap->width;
	unsigned int h = bitmap->rows;
	unsigned char buffer[w * h];

	BinPack2D::Size size = {
		bitmap->width,
		bitmap->rows
	};

	TextureAtlas::GlyphInfo data = {
		bitmap_glyph->left,
		bitmap_glyph->top,
		(int)bitmap_glyph->root.advance.x,
		(int)bitmap_glyph->root.advance.y,
		glyph_size
	};

	ret = atlas->insert(glyph_index, size, data, position);
	if (!ret)
		return 0;

	for (i = 0; i < h; i++) {
		for (j = 0; j < w; j++) {
			if (bitmap->pixel_mode == FT_PIXEL_MODE_MONO) {
				buffer[i*w + j] =
					(bitmap->buffer[i*bitmap->pitch + j/8] & (1 << (7 - j%8)))
					? 0xFF : 0;
			} else if (bitmap->pixel_mode == FT_PIXEL_MODE_GRAY) {
				buffer[i*w + j] = bitmap->buffer[i*bitmap->pitch + j];
			}
		}
	}

	atlas->updatePartTexture(position.x, position.y, size.w, size.h,
		GL_RED, GL_UNSIGNED_BYTE, buffer);

	return 1;
}

int Font::generic_font_draw_text(bool draw,
				   int *height, int x, int y,
				   unsigned int color,
				   unsigned int size,
				   const char *text)
{
	const FT_ULong flags = FT_LOAD_RENDER | FT_LOAD_TARGET_NORMAL;
	FT_Face face;
	FT_Int charmap_index;
	FT_Glyph glyph;
	FT_UInt glyph_index;
	FT_Bool use_kerning;
	FTC_FaceID face_id = (FTC_FaceID)this;
	FT_UInt previous = 0;

	unsigned int character;
	int start_x = x;
	int max_x = 0;
	int pen_x = x;
	int pen_y = y;
	BinPack2D::Rectangle rect;
	TextureAtlas::GlyphInfo glyph_info;

	FTC_ScalerRec scaler;
	scaler.face_id = face_id;
	scaler.width = size;
	scaler.height = size;
	scaler.pixel = 1;

	FTC_Manager_LookupFace(ftcmanager, face_id, &face);
	use_kerning = FT_HAS_KERNING(face);
	charmap_index = FT_Get_Charmap_Index(face->charmap);

	while (text[0]) {
		//character = utf8_character(&text);
		character = *text;
		text++;

		if (character == '\n') {
			if (pen_x > max_x)
				max_x = pen_x;
			pen_x = start_x;
			pen_y += size;
			continue;
		}

		glyph_index = FTC_CMapCache_Lookup(cmapcache,
						   face_id,
						   charmap_index,
						   character);

		if (use_kerning && previous && glyph_index) {
			FT_Vector delta;
			FT_Get_Kerning(face, previous, glyph_index,
				       FT_KERNING_DEFAULT, &delta);
			pen_x += delta.x >> 6;
		}

		if (!tex_atlas->get(glyph_index, &rect, &glyph_info)) {
			FTC_ImageCache_LookupScaler(imagecache,
						    &scaler,
						    flags,
						    glyph_index,
						    &glyph,
						    NULL);

			if (!atlas_add_glyph(tex_atlas, glyph_index,
					     (FT_BitmapGlyph)glyph, size)) {
				continue;
			}

			if (!tex_atlas->get(glyph_index, &rect, &glyph_info))
				continue;
		}

		const float draw_scale = size / (float)glyph_info.glyph_size;

		if (draw) {
			glBindBuffer(GL_ARRAY_BUFFER, texture_vbo[0]);
			{
				float draw_x = pen_x + glyph_info.bitmap_left * draw_scale;
				float draw_y = pen_y - glyph_info.bitmap_top * draw_scale;

				GLfloat vertices[2 * 4] = {
					draw_x, draw_y,
					draw_x, draw_y + rect.h,
					draw_x + rect.w, draw_y,
					draw_x + rect.w, draw_y + rect.h,
				};
				glBufferSubData(GL_ARRAY_BUFFER,
					0,
					sizeof(vertices),
					vertices);
			}
			glBindBuffer(GL_ARRAY_BUFFER, texture_vbo[1]);
			{
				float atlas_w = tex_atlas->getTextureWidth();
				float atlas_h = tex_atlas->getTextureHeight();

				GLfloat texcoords[2 * 4] = {
					rect.x / atlas_w, (rect.y) / atlas_h,
					rect.x / atlas_w, (rect.y + rect.h) / atlas_h,
					(rect.x + rect.w) / atlas_w, (rect.y) / atlas_h,
					(rect.x + rect.w) / atlas_w, (rect.y + rect.h) / atlas_h,
				};
				glBufferSubData(GL_ARRAY_BUFFER,
					0,
					sizeof(texcoords),
					texcoords);
			}

			tex_atlas->bindTexture(GL_TEXTURE_2D);
			glBindVertexArray(texture_vao);
				glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
			glBindVertexArray(0);
		}

		pen_x += (glyph_info.advance_x >> 16) * draw_scale;
	}

	if (pen_x > max_x)
		max_x = pen_x;

	if (height)
		*height = pen_y + size - y;

	return max_x - x;
}

int Font::drawText(int x, int y, unsigned int color, unsigned int size, const char *text)
{
	return generic_font_draw_text(true, nullptr, x, y, color, size, text);
}
